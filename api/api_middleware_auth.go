package main

import (
	"net/http"
	"strings"

	"gitlab.com/albttx/poc-jwt-auth/api/internal/auth"

	"github.com/labstack/echo"
)

func authMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		bearer := ctx.Request().Header.Get("Authorization")
		if !strings.HasPrefix(bearer, "Bearer ") {
			return echo.NewHTTPError(http.StatusBadRequest, "missing Bearer in token")
		}
		bearer = strings.TrimPrefix(bearer, "Bearer ")

		// jwt access token validation
		claims, err := auth.ParseJWTToken(bearer)
		if err != nil {
			return echo.ErrBadRequest
		}

		ctx.Set("user_id", claims.UserID)

		if err := next(ctx); err != nil {
			ctx.Error(err)
		}

		return nil
	}
}
