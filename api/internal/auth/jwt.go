package auth

import (
	"bytes"
	"os"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/juju/errors"
	"github.com/sirupsen/logrus"
)

var (
	// TODO: USE ENV
	JWT_SECRET              = []byte(os.Getenv("JWT_SECRET"))
	DEFAULT_EXPIRATION_TIME = time.Hour * 24 * 7
)

func init() {
	if bytes.Compare(JWT_SECRET, []byte{}) == 0 {
		logrus.Warn("JWT_SECRET is not set")
		JWT_SECRET = []byte("unsafe_jwt_secret")
	}
}

type Claims struct {
	jwt.StandardClaims

	// In production use UUIDv4
	UserID int `json:"user_id"`

	Username string `json:"username"`
}

// GenerateJWTToken ...
func GenerateJWTToken(claims Claims) (string, error) {
	expireDate := time.Now().Add(DEFAULT_EXPIRATION_TIME)
	claims.StandardClaims = jwt.StandardClaims{
		ExpiresAt: expireDate.Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(JWT_SECRET)
	if err != nil {
		return "", errors.Trace(err)
	}
	return tokenString, nil
}

// ParseJWTToken ...
func ParseJWTToken(tokenString string) (*Claims, error) {
	claims := &Claims{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	token, err := jwt.ParseWithClaims(tokenString, claims, func(_ *jwt.Token) (interface{}, error) {
		return JWT_SECRET, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return nil, errors.NewUnauthorized(err, "invalid jwt token signature")
		}
		return nil, errors.NewBadRequest(err, "failed to parse claims")
	}
	if !token.Valid {
		return nil, errors.NewUnauthorized(err, "invalid jwt token")
	}
	return claims, nil
}

// RefreshJWTToken ...
func RefreshJWTToken(tokenString string) (string, error) {
	claims, err := ParseJWTToken(tokenString)
	if err != nil {
		return tokenString, err
	}
	tokenString, err = GenerateJWTToken(*claims)
	if err != nil {
		return tokenString, err
	}
	return tokenString, nil
}
