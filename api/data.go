package main

type user struct {
	ID       int    `json:"id,omitempty"`
	Name     string `json:"name,omitempty"`
	Password string `json:"password,omitempty"`
}

// map [ id ] user{}
var dbUser = map[int]user{
	1: {
		Name:     "admin",
		Password: "admin",
	},
	2: {
		Name:     "nikpol",
		Password: "password",
	},
}

func getUserByName(name string) *user {
	for i, u := range dbUser {
		if u.Name == name {
			u.ID = i
			return &u
		}
	}
	return nil
}
