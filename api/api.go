package main

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"gitlab.com/albttx/poc-jwt-auth/api/internal/auth"
)

type service interface {
	LoginHandler(ctx echo.Context) error
	GetUserHandler(ctx echo.Context) error
}

func newService() service {
	return &client{}
}

type client struct {
}

func (c client) LoginHandler(ctx echo.Context) error {
	type Request struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	req := Request{}

	if err := ctx.Bind(&req); err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "Please provide valid credentials")
	}

	user := getUserByName(req.Username)
	if user == nil {
		return echo.NewHTTPError(http.StatusNotFound, "User not found")
	}

	if user.Password != req.Password {
		return echo.NewHTTPError(http.StatusUnauthorized, "Bad username / password")
	}

	jwtToken, err := auth.GenerateJWTToken(auth.Claims{
		UserID:   user.ID,
		Username: user.Name,
	})
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	logrus.Infof("user %v logged in", user.Name)
	return ctx.JSON(http.StatusOK, echo.Map{
		"token": jwtToken,
	})
}

func (c client) GetUserHandler(ctx echo.Context) error {
	userID := ctx.Get("user_id").(int)

	user := dbUser[userID]
	// don't return password
	user.Password = ""

	logrus.Infof("Getting user: %v", user)

	return ctx.JSON(200, echo.Map{
		"user": user,
	})
}
