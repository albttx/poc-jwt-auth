package main

import (
	"net/http"

	"github.com/caarlos0/env"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/sirupsen/logrus"
)

type config struct {
	Host string `env:"HOST" envDefault:"0.0.0.0"`
	Port string `env:"PORT" envDefault:"3000"`
}

func main() {
	cfg := config{}
	if err := env.Parse(&cfg); err != nil {
		logrus.WithError(err).Fatal()
	}

	e := echo.New()

	e.Use(
		// CORS middlewares
		middleware.CORSWithConfig(middleware.CORSConfig{
			AllowOrigins: []string{"*"},
			AllowHeaders: []string{"*"},
			AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete},
		}),
	)

	serviceAPI := newService()

	e.POST("/login", serviceAPI.LoginHandler)
	e.GET("/users/me", serviceAPI.GetUserHandler, authMiddleware)

	if err := e.Start(cfg.Host + ":" + cfg.Port); err != nil {
		logrus.WithError(err).Fatal()
	}
}
