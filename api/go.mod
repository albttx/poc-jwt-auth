module gitlab.com/albttx/poc-jwt-auth/api

go 1.15

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/juju/errors v0.0.0-20200330140219-3fe23663418f
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.2.0
	github.com/sirupsen/logrus v1.7.0
)
