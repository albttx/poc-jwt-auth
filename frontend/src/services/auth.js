import axios from "axios";

export const isBrowser = () => typeof window !== "undefined";

export const getToken = () =>
  isBrowser() && window.localStorage.getItem("jwt_token");

const setToken = (token) =>
  isBrowser() && window.localStorage.setItem("jwt_token", token);

export const handleLogin = async ({ username, password }) => {
  try {
    const resp = await axios.post("http://localhost:3000/login", {
      username,
      password,
    });
    const token = resp.data && resp.data.token;
    console.log("[DEBUG] JWT TOKEN", token);
    return setToken(token);
  } catch (err) {
    console.error(err);
    return false;
  }

  return false;
};

export const isLoggedIn = () => {
  return !!getToken();
};

export const logout = (callback) => {
  setToken({});
  callback();
};
