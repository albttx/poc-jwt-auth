import axios from "axios";

import { getToken } from "./auth";

export const getUser = async () => {
  try {
    const resp = await axios.get("http://localhost:3000/users/me", {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    });
    const user = resp.data && resp.data.user;
    console.log("[DEBUG] User", user);
    return user;
  } catch (err) {
    console.error(err);
    return false;
  }
};
