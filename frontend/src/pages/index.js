import React from "react";
import { navigate } from "gatsby";
import { isLoggedIn } from "../services/auth";
import { getUser } from "../services/user";

class IndexPage extends React.Component {
  user = getUser();

  render() {
    if (!isLoggedIn()) {
      navigate(`/login`);
    }
    return (
      <>
        <h1>Hello {this.user.name}</h1>
      </>
    );
  }
}
export default IndexPage;
